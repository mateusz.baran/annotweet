import React from 'react';
import './App.css';
import MainScreen from "./MainScreen";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

function App() {
    const isDesktop = useMediaQuery('(min-width:1025px)');
    return (
        <div className="app-container">
            <MainScreen isDesktop={isDesktop}/>
        </div>
    );
}

export default App;
