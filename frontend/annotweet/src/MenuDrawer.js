import React from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import Drawer from "@material-ui/core/Drawer/Drawer";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Divider from "@material-ui/core/Divider/Divider";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import Typography from "@material-ui/core/Typography/Typography";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
    alignRight: {
        textAlign: 'right'
    },
    drawerBackground: '#0101ff',
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    item: {
        paddingBottom: 0
    }
}));

export default function MenuDrawer(props) {
    const classes = useStyles();
    const theme = useTheme();
    const isDesktop = useMediaQuery('(min-width:1025px)');

    const userStats = ['You', 'Inducement', 'Encouragement', 'VotingTurnout', 'Normal', 'Skip', 'Total'];
    const summaryStats = ['Summary', 'Annotations', 'Inducement', 'Encouragement', 'VotingTurnout', 'Normal',
        'Skip', 'Done', 'Doing', 'Todo', 'Total', 'CohenKappa'];

    return (
        <React.Fragment>
            <div className={classes.root}>
                <CssBaseline/>
                <Drawer
                    className={classes.drawer}
                    variant={isDesktop ? "permanent" : "persistent"}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    open={props.open}
                    anchor="left"
                >
                    <div className={classes.drawerHeader}>
                        {isDesktop
                            ? null
                            : (<IconButton onClick={props.handleDrawerClose}>
                                    {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                                </IconButton>
                            )}
                    </div>
                    <Divider/>
                    <List>
                        {userStats.map((text, index) => (
                            <ListItem key={text} classes={{root: classes.item}}>
                                {index === 0
                                    ? (<ListItemText disableTypography
                                                     primary={<Typography type="body2"
                                                                          style={{
                                                                              color: 'black',
                                                                              fontWeight: 'bold'
                                                                          }}>{text}</Typography>}/>)
                                    : (<ListItemText primary={text}/>)}
                                {index !== 0 && <ListItemText primary={props[`${text.toLowerCase()}`]}
                                                              className={classes.alignRight}/>}
                            </ListItem>
                        ))}
                    </List>
                    <Divider/>
                    <List>
                        {summaryStats.map((text, index) => (
                            <ListItem key={text} classes={{root: classes.item}}>
                                {index === 0
                                    ? (<ListItemText disableTypography
                                                     primary={<Typography type="body2"
                                                                          style={{
                                                                              color: 'black',
                                                                              fontWeight: 'bold'
                                                                          }}>{text}</Typography>}/>)
                                    : (<ListItemText primary={text}/>)}
                                {index !== 0 &&
                                <ListItemText primary={props[`total${text}Count`].toFixed(4).replace(/\.0+$/,'')} className={classes.alignRight}/>}
                            </ListItem>
                        ))}
                    </List>
                </Drawer>
            </div>
            <div className={classes.drawerHeader}/>
        </React.Fragment>
    );
}
