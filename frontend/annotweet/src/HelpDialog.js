import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const {children, classes, onClose, ...other} = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function HelpDialog(props) {
    return (
        <div>
            <Dialog onClose={props.toggleDialog} aria-labelledby="customized-dialog-title" open={props.open}>
                <DialogTitle id="customized-dialog-title" onClose={props.toggleDialog}>
                    Annotweet - help
                </DialogTitle>
                <DialogContent dividers>

                    <Typography variant="h4" gutterBottom>
                        Agitacja - kategorie
                    </Typography>
                    <Typography variant="h6" gutterBottom>
                        Przyjęte zostały 4 kategorie główne, które reprezentowane są przez przyciski na dole strony:
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Nakłanianie - jawne przekonywanie lub odradzanie do głosowania na daną osobę. Z treści musi
                        wynikać jaką osobę dotyczy tekst.
                        Jest to najbardziej intuicyjna kategoria, do której zalicza się agitacja w klasycznym ujęciu -
                        np.: "Głosujcie na Dudę!" lub
                        "Polacy, wybierzcie dobrze, ten kraj potrzebuje takiego prezydenta jak Rafał."
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Zachęcanie/zniechęcanie - przypisywanie dobrych lub złych skojarzeń, cech do danej osoby.
                        Ta kategoria zawiera tweety, które odnoszą się do kandydatów lub partii bezpośrednio, ale nie są
                        klasyczną agitacją.
                        Są to najczęściej pozytywne lub negatywne wypowiedzi na temat politykow - np.: "Trzaskowski to
                        kłamca!" lub
                        "Nie mogą słuchać tego człowieka, Duda jesteś pupilkiem prezesa i nie reprezentujesz sobą nic"
                        lub
                        "Ten Hołownia robi naprawdę dobre wrażenie, może w końcu to jakaś nowa nadzieja dla Polski".
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Agitacja profrekwencyjna/antyfrekwencyjna - dotyczy się namawiania/odradzania do samego udziału
                        w głosowaniu
                        np.: "Polacy, idźcie na wybory!" lub "Byłem na wyborach o 8 rano i nie było nikogo, nie pozwólmy
                        żeby tak było"
                        lub "Mam gdzieś to wszystko i tak wyjadę za granice, szkoda czasu na marnowanie głosu"
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Nieagitacja - tekst neutralny bądź niekwalifikujący się do pozostałych grup. Normalna wypowiedź
                        lub element
                        dyskusji, który nie zalicza się do żadnej z powyższych grup.
                        np.: "Ciekaw jestem tych wyborów, walka będzie toczyć się do samego końca" lub
                        "PiS wprowadził nową ustawę, która nie podoba się wielu ludziom. Co o tym myślicie?"
                        lub "Odsunięcie przez PIS, PKW od wyborów, w 2 ustawie o Covid 19, miało być sprytną zasadzką
                        zastawioną
                        na opozycję i Senat, a okazało się pułapką, w którą wpada PIS."
                    </Typography>


                    <Typography variant="h4" gutterBottom>
                        Informacje dodatkowe
                    </Typography>
                    <Typography variant="h6" gutterBottom>
                        Dodatkowo zaznaczyć można 3 opcje, które mogą łączyć się ze wszystkimi kategoriami agitacji.
                        Każdy tweet może mieć od 0 do 3 przypisanych dodatkowych informacji (żadna, 1, 2 lub wszystkie):
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Satyra - wypowiedź mająca formę satyry. Celowa zmiana kandydata/partii na określenie satyryczne
                        (np. Duda - długopis itd.)
                        Różnego rodzaju wierszyki, rymowanki, wypowiedzi jawnie wskazujące na tzw. bait - np.
                        "Trzaśnijmy krzyżyk, niech coś się zmieni" lub
                        "PiS jest wielki jak dwie butelki, Andrzej moim wodzem".
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Kontekst (brak kontekstu) - tekst wymaga dodatkowej wiedzy aby przypisać go do danej kategorii.
                        Można o tym pomyśleć w stylu: "Wiem, że to agitacja, ale dlatego, że posiadam pewną wiedzę,
                        która bezpośrednio nie wynika z tekstu"
                        lub "Ja to rozumiem, ale po samym tekście trudno określić o co chodzi".
                        Są to często trudne do wyłapania aluzje lub wypowiedzi, które odnoszą się do linku, czy
                        załącznika, o którym nie wiemy,
                        a w nim tkwi odpowiedź. - np. "Niech on już skończy, nie mogę tego znieść tego steku bzdur..." +
                        link.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Doniesienie medialne - tekst pochodzący od podmiotu medialnego np. telewizja, radio, gazeta,
                        portal. Tekst w założeniu ma cel informacyjny; najczęściej cytat, parafraza, bądź przytoczenie
                        wypowiedzi - np. "#czasdecyzji | Dzisiaj naszym gościem będzie minister Łukasz Szumowski" lub
                        "Znany poseł platformy o Kaczyńskim: kiedyś był moim przyjacielem. Czytaj więcej: https://...."
                    </Typography>


                    <Typography variant="h4" gutterBottom>
                        Uwaga
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Kliknięcie na przycisk dotyczący kategorii agitacji powoduje przejście do następnego tweeta.
                        Żeby oznaczyć informacje
                        dodatkowe należy zaznaczyć je PRZED kliknięciem w jeden z kolorowych przycisków (najpierw
                        zaznaczamy informacje dodatkowe - Satyra/Kontekst/Doniesienie medialne,
                        a następnie oznaczamy rodzaj agitacji)
                    </Typography>


                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={props.toggleDialog} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}