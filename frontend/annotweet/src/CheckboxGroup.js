import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

const DESKTOP_CONTROL_MARGIN = '30px';
const MOBILE_CONTROL_MARGIN = '20px';

export default function CheckboxGroup(props) {

    const handleChange = (event) => {
        props.toggleCheckbox(event.target.name);
    };

    let isDesktop = props.isDesktop;
    let controlMarginRight = isDesktop ? DESKTOP_CONTROL_MARGIN : MOBILE_CONTROL_MARGIN;

    return (
        <div id='annot-checkboxes' style={{bottom: props.cssBottom}}>
            <FormGroup row>
                <Tooltip
                    title="SATIRE / Wypowiedź mająca formę satyry. Tekst zawiera inne przesłanie niż zawarte w nim słowa"
                    placement="top">
                    <FormControlLabel
                        control={<Checkbox checked={props.satireChecked} onChange={handleChange} name="satire"
                                           style={{
                                               transform: "scale(2)",
                                               marginRight: '8px',
                                               marginLeft: '5px'
                                           }}/>}
                        label="Satire"
                        style={{
                            border: '1px solid gray',
                            borderRadius: '5px',
                            padding: '6px',
                            marginRight: controlMarginRight
                        }}
                    />
                </Tooltip>
                <Tooltip title="MISSING CONTEXT / Tekst wymaga dodatkowej wiedzy, aby przypisać do do danej kategorii"
                         placement="top">
                    <FormControlLabel
                        control={<Checkbox checked={props.missingContextChecked} onChange={handleChange}
                                           name="missingContext"
                                           style={{
                                               transform: "scale(2)",
                                               marginRight: '8px',
                                               marginLeft: '5px'
                                           }}/>}
                        label="Context"
                        style={{
                            border: '1px solid gray',
                            borderRadius: '5px',
                            padding: '6px',
                            marginRight: controlMarginRight
                        }}
                    />
                </Tooltip>
                <Tooltip
                    title="MEDIA REPORT / Test pochodzący z mediów. W założeniu ma cel informacyjny. Naczęściej cytat, parafraza, bądź przytoczenie wypowiedzi"
                    placement="top">
                    <FormControlLabel
                        control={<Checkbox checked={props.mediaReportChecked} onChange={handleChange} name="mediaReport"
                                           style={{
                                               transform: "scale(2)",
                                               marginRight: '8px',
                                               marginLeft: '5px'
                                           }}/>}
                        label="Media"
                        style={{
                            border: '1px solid gray',
                            borderRadius: '5px',
                            padding: '6px'
                        }}
                    />
                </Tooltip>
            </FormGroup>
        </div>
    );
}