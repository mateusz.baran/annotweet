import React, {Component} from 'react';
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";

class DialogUsername extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            errorText: ''
        }
    }

    onNameChange(event) {
        let username = event.target.value;
        if (username) {
            this.setState({
                username: username,
                errorText: ''
            })
        } else {
            this.setState({
                username: username,
                errorText: 'Username cannot be empty'
            })
        }
    }

    handleOkButtonClick() {
        if (this.state.username.trim()) {
            this.props.handleSubmitUsername(this.state.username);
            this.props.handleClose();
        }
    }

    render() {
        return (
            <Dialog open={this.props.open} onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title" disableBackdropClick={true} disableEscapeKeyDown={true}>
                <DialogTitle id="form-dialog-title">Username</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Enter your username
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Username"
                        type="text"
                        value={this.state.username}
                        error={!this.state.username}
                        helperText={this.state.errorText}
                        fullWidth
                        onChange={this.onNameChange.bind(this)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleOkButtonClick.bind(this)}
                            color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default DialogUsername;