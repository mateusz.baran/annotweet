import React, {Component} from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import ButtonAppBar from "./AppBar";
import './MainScreen.css';
import DialogUsername from "./DialogUsername";
import {getCookie, setCookie} from "./utils";
import HelpDialog from "./HelpDialog";
import {DEVELOPMENT_HOST, MODE, PRODUCTION_HOST} from "./settings";
import ClearIcon from '@material-ui/icons/Clear';
import HowToVoteIcon from '@material-ui/icons/HowToVote';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import CardElement from "./Card";
import CheckboxGroup from "./CheckboxGroup";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

let isDevelopment = MODE === 'development';
console.log(`Development mode on: ${isDevelopment}`);

const HOST = isDevelopment ? DEVELOPMENT_HOST : PRODUCTION_HOST;

const ENDPOINT_NEXT_TWEET = `${HOST}/text/get`;
const ENDPOINT_ANNOTATE_TWEET = `${HOST}/text/annotate`;
const ENDPOINT_ANNOTATATION_STATS = `${HOST}/text/annotation_stats`;
const ENDPOINT_STATS = `${HOST}/user/stats`;

const ANNOTATION_INDUCEMENT = 'inducement';
const ANNOTATION_ENCOURAGEMENT = 'encouragement';
const ANNOTATION_VOTING_TURNOUT = 'voting_turnout';
const ANNOTATION_NORMAL = 'normal';
const ANNOTATION_SKIP = 'skip';

const LABEL_SATIRE = 'satire';
const LABEL_MISSING_CONTEXT = 'missing_context';
const LABEL_MEDIA_REPORT = 'media_report';

const CHECKBOX_STATE_NAMES = {
    'satireChecked': LABEL_SATIRE,
    'missingContextChecked': LABEL_MISSING_CONTEXT,
    'mediaReportChecked': LABEL_MEDIA_REPORT
};

const COOKIE_USERNAME_NAME = 'annotweet-username-cookie';
const COOKIE_USERNAME_EXPIRE_DAYS = 365;

const KEY_1 = [49, 97];
const KEY_2 = [50, 98];
const KEY_3 = [51, 99];
const KEY_4 = [52, 100];
const KEY_5 = [53, 101];
const KEY_Q = [81];
const KEY_W = [87];
const KEY_E = [69];

const CANNOT_LOAD_TWEET_TEXT = 'Nie udało sie załadować tweeta - zaczekaj chwilę i odśwież stronę';

const BUTTON_WIDTH = 80;
const BUTTON_HEIGHT = 80;
const BUTTON_MARGIN_BOTTOM = 30;
const BUTTON_MARGIN_RIGHT = 30;
const BUTTON_MARGIN_RIGHT_MOBILE = 15;

class MainScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: getCookie(COOKIE_USERNAME_NAME),
            tweetId: null,
            tweetText: '',
            lastTweetId: null,
            lastTweetText: null,
            nextTweetId: null,
            nextTweetText: null,
            loadNextTweetInProgress: false,
            dialogOpen: getCookie(COOKIE_USERNAME_NAME) === null,
            helpOpen: false,
            drawerOpen: false,
            annotButtonsDisabled: false,
            // User stats
            inducement: 0,
            encouragement: 0,
            votingturnout: 0,
            normal: 0,
            skip: 0,
            total: 0,
            // Summary stats
            totalAnnotationsCount: 0,
            totalInducementCount: 0,
            totalEncouragementCount: 0,
            totalVotingTurnoutCount: 0,
            totalNormalCount: 0,
            totalSkipCount: 0,
            totalDoneCount: 0,
            totalDoingCount: 0,
            totalTodoCount: 0,
            totalTotalCount: 0,
            totalCohenKappaCount: 0,
            // Labels
            satireChecked: false,
            missingContextChecked: false,
            mediaReportChecked: false
        };

        this.getLabels = this.getLabels.bind(this);
    }

    _handleKeyDown = (event) => {
        if (this.state.dialogOpen || this.state.annotButtonsDisabled)
            return;

        if (KEY_1.includes(event.keyCode)) {
            this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_INDUCEMENT);
        }
        else if (KEY_2.includes(event.keyCode)) {
            this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_ENCOURAGEMENT);
        }
        else if (KEY_3.includes(event.keyCode)) {
            this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_VOTING_TURNOUT);
        }
        else if (KEY_4.includes(event.keyCode)) {
            this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_NORMAL);
        }
        else if (KEY_5.includes(event.keyCode)) {
            this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_SKIP);
        }
        else if (KEY_Q.includes(event.keyCode)) {
            this.toggleCheckbox('satire');
        }
        else if (KEY_W.includes(event.keyCode)) {
            this.toggleCheckbox('missingContext');
        }
        else if (KEY_E.includes(event.keyCode)) {
            this.toggleCheckbox('mediaReport');
        }
    };

    componentDidMount() {
        document.addEventListener("keydown", this._handleKeyDown);
        if (!this.state.tweetId) {
            this.getNextTweet();
            this.getStats();
            this.getSummaryStats();
        }
    }

    clearTweetState() {
        this.setState({
            annotButtonsDisabled: false,
            satireChecked: false,
            missingContextChecked: false,
            mediaReportChecked: false
        })
    }

    getNextTweet() {
        let lastTweetId = this.state.tweetId;
        let lastTweetText = this.state.tweetText;
        axios.post(ENDPOINT_NEXT_TWEET, {username: this.state.username},
            {headers: {'Content-Type': 'application/x-www-form-urlencoded', "accept": "application/json"}})
            .then(response => {
                let tweetId = response.data._id;
                let tweetText = response.data.text;
                this.setState({
                    lastTweetId: lastTweetId,
                    lastTweetText: lastTweetText,
                    tweetId: tweetId,
                    tweetText: tweetText,
                });
            })
            .catch(error => {
                this.setState({
                    tweetId: null,
                    tweetText: '',
                    lastTweetId: lastTweetId,
                    lastTweetText: lastTweetText,
                });
                if (error.response.status === 404) {
                    this.handleNotFound();
                }
            })
            .finally(() => {
                this.clearTweetState();
            });
    }

    cacheNextTweet() {
        this.setState({
            loadNextTweetInProgress: true
        }, () => {
            axios.post(ENDPOINT_NEXT_TWEET, {username: this.state.username},
                {headers: {'Content-Type': 'application/x-www-form-urlencoded', "accept": "application/json"}})
                .then(response => {
                    let tweetId = response.data._id;
                    let tweetText = response.data.text;
                    this.setState({
                        nextTweetId: tweetId,
                        nextTweetText: tweetText,
                    });
                })
                .catch(error => {
                    this.setState({
                        nextTweetId: null,
                        nextTweetText: null,
                    });
                    if (error.response.status === 404) {
                        this.handleNotFound();
                    }
                })
                .finally(() => {
                    this.setState({
                        loadNextTweetInProgress: false
                    })
                });
        })
    }

    loadCachedTweet(retry) {
        if (retry && this.state.loadNextTweetInProgress) {
            console.log('Loading... Set timeout');
            setTimeout(() => this.loadCachedTweet(true), 100);
        } else {
            let lastTweetId = this.state.tweetId;
            let lastTweetText = this.state.tweetText;
            let tweetId = this.state.nextTweetId;
            let tweetText = this.state.nextTweetText;
            this.setState({
                lastTweetId: lastTweetId,
                lastTweetText: lastTweetText,
                tweetId: tweetId,
                tweetText: tweetText,
                nextTweetId: null,
                nextTweetText: null
            }, () => {
                this.clearTweetState();
            });
        }
    }

    sendAnnotMessage(tweetId, annotation) {
        console.log(annotation, this.getLabels());
        let endpoint = `${ENDPOINT_ANNOTATE_TWEET}/${tweetId}`;
        axios.patch(endpoint, {username: this.state.username, answer: annotation, labels: this.getLabels()},
            {headers: {'Content-Type': 'application/x-www-form-urlencoded', "accept": "application/json"}})
            .then(response => {

            })
            .catch(error => {
                console.log(error);
            });
    }

    getStats() {
        axios.post(ENDPOINT_STATS, {username: this.state.username},
            {headers: {'Content-Type': 'application/x-www-form-urlencoded', "accept": "application/json"}})
            .then(response => {
                this.setState({
                    inducement: response.data.inducement,
                    encouragement: response.data.encouragement,
                    votingturnout: response.data.voting_turnout,
                    normal: response.data.normal,
                    skip: response.data.skip,
                    total: response.data.total,
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    getSummaryStats() {
        axios.get(ENDPOINT_ANNOTATATION_STATS,
            {headers: {'Content-Type': 'application/x-www-form-urlencoded', "accept": "application/json"}})
            .then(response => {
                this.setState({
                    totalAnnotationsCount: response.data.annotations,
                    totalInducementCount: response.data.inducement,
                    totalEncouragementCount: response.data.encouragement,
                    totalVotingTurnoutCount: response.data.voting_turnout,
                    totalNormalCount: response.data.normal,
                    totalSkipCount: response.data.skip,
                    totalDoneCount: response.data.done,
                    totalDoingCount: response.data.doing,
                    totalTodoCount: response.data.todo,
                    totalTotalCount: response.data.total,
                    totalCohenKappaCount: response.data.cohen_kappa
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    onAnnotButtonClick(tweetId, annotation) {
        if (!this.state.annotButtonsDisabled) {
            this.setState({
                annotButtonsDisabled: true
            }, () => {
                this.sendAnnotMessage(tweetId, annotation);
                let canLoadCachedTweet = !this.state.loadNextTweetInProgress
                    && this.state.nextTweetText !== null
                    && this.state.nextTweetId !== null;
                if (canLoadCachedTweet)
                    this.loadCachedTweet(false);
                else if (this.state.loadNextTweetInProgress)
                    this.loadCachedTweet(true);
                else
                    this.getNextTweet();

                this.getStats();
                this.getSummaryStats();
                if (!this.state.loadNextTweetInProgress)
                    this.cacheNextTweet();
            });
        }
    }

    handleSubmitUsername(username) {
        this.setState({
            username: username
        }, () => {
            this.getNextTweet();
            this.getStats();
        });
        setCookie(COOKIE_USERNAME_NAME, username, COOKIE_USERNAME_EXPIRE_DAYS)
    }

    handleOpenUsernameDialog() {
        this.setState({
            dialogOpen: true
        })
    }

    handleCloseUsernameDialog() {
        this.setState({
            dialogOpen: false
        })
    }

    toggleHelpDialog() {
        let prevState = this.state.helpOpen;
        this.setState({
            helpOpen: !prevState
        })
    }

    handleDrawerOpen() {
        this.setState({
            drawerOpen: true
        });
    }

    handleDrawerClose() {
        this.setState({
            drawerOpen: false
        })
    }

    handleNotFound() {
        this.setState({
            tweetId: null,
            tweetText: CANNOT_LOAD_TWEET_TEXT
        })
    }

    toggleCheckbox(checkbox) {
        let lastState = this.state[`${checkbox}Checked`];
        this.setState({
            [`${checkbox}Checked`]: !lastState
        })
    }

    getLabels() {
        let labels = [];
        for (let name in CHECKBOX_STATE_NAMES) {
            if (this.state[name]) {
                labels.push(CHECKBOX_STATE_NAMES[name]);
            }
        }
        return labels;
    }

    onBackToLastTweetButtonClick() {
        this.clearTweetState();

        let lastId = this.state.lastTweetId;
        let lastText = this.state.lastTweetText;
        this.setState({
            tweetId: lastId,
            tweetText: lastText
        })
    }

    render() {
        let buttonMargin = this.props.isDesktop ? BUTTON_MARGIN_RIGHT : BUTTON_MARGIN_RIGHT_MOBILE;

        return (
            <div id="content-wrapper">
                <div id='content-top'>
                    <ButtonAppBar username={this.state.username}
                                  onHelpButtonClick={() => this.toggleHelpDialog()}
                                  onUsernameButtonClick={() => this.handleOpenUsernameDialog()}
                                  onBackToLastTweetButtonClick={() => this.onBackToLastTweetButtonClick()}
                                  drawerOpen={this.state.drawerOpen}
                                  handleDrawerOpen={this.handleDrawerOpen.bind(this)}
                                  handleDrawerClose={this.handleDrawerClose.bind(this)}
                                  mainState={this.state}
                    />
                </div>
                <div id='content-screen'>
                    <div id='annot-tweet'>
                        {this.state.annotButtonsDisabled ? (<LinearProgress/>) :
                            <CardElement tweetId={this.state.tweetId} tweetText={this.state.tweetText}
                                         errorText={CANNOT_LOAD_TWEET_TEXT}/>
                        }
                    </div>
                    <div id='annot-buttons'>
                        <CheckboxGroup toggleCheckbox={this.toggleCheckbox.bind(this)}
                                       satireChecked={this.state.satireChecked}
                                       missingContextChecked={this.state.missingContextChecked}
                                       mediaReportChecked={this.state.mediaReportChecked}
                                       isDesktop={this.props.isDesktop}
                                       cssBottom={`${BUTTON_MARGIN_BOTTOM + BUTTON_HEIGHT}px`}/>
                        <Tooltip title="INDUCEMENT / Nakłanianie do głosowania na określonego kandydata lub partię"
                                 placement="top">
                            <Button variant="contained" color="secondary" size="large"
                                    onClick={() => this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_INDUCEMENT)}
                                    style={{
                                        backgroundColor: "#009a00",
                                        marginRight: `${buttonMargin}px`,
                                        width: `${BUTTON_WIDTH}px`,
                                        height: `${BUTTON_HEIGHT}px`
                                    }}
                                    disabled={this.state.annotButtonsDisabled}>
                                <RecordVoiceOverIcon fontSize="large"/>
                            </Button>
                        </Tooltip>
                        <Tooltip title="ENCOURAGEMENT / Zachęcanie lub zniechęcanie do określonego kandydata lub partii"
                                 placement="top">
                            <Button variant="contained" color="secondary" size="large"
                                    onClick={() => this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_ENCOURAGEMENT)}
                                    style={{
                                        backgroundColor: "#2196f3",
                                        marginRight: `${buttonMargin}px`,
                                        width: `${BUTTON_WIDTH}px`,
                                        height: `${BUTTON_HEIGHT}px`
                                    }}
                                    disabled={this.state.annotButtonsDisabled}>
                                <ThumbsUpDownIcon fontSize="large"/>
                            </Button>
                        </Tooltip>
                        <Tooltip title="VOTING TURNOUT / Agitacja profrekwencyjna lub antyfrekwencyjna"
                                 placement="top">
                            <Button variant="contained" color="secondary" size="large"
                                    onClick={() => this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_VOTING_TURNOUT)}
                                    style={{
                                        backgroundColor: "#ff9800",
                                        marginRight: `${buttonMargin}px`,
                                        width: `${BUTTON_WIDTH}px`,
                                        height: `${BUTTON_HEIGHT}px`
                                    }}
                                    disabled={this.state.annotButtonsDisabled}>
                                <HowToVoteIcon fontSize="large"/>
                            </Button>
                        </Tooltip>
                        <Tooltip title="NORMAL / Nieagitacja lub normalna wypowiedź"
                                 placement="top">
                            <Button variant="contained" color="secondary" size="large"
                                    onClick={() => this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_NORMAL)}
                                    style={{
                                        marginRight: `${buttonMargin}px`,
                                        width: `${BUTTON_WIDTH}px`,
                                        height: `${BUTTON_HEIGHT}px`
                                    }}
                                    disabled={this.state.annotButtonsDisabled}>
                                <ClearIcon fontSize="large"/>
                            </Button>
                        </Tooltip>
                        <Tooltip title="SKIP / Pomiń jeśli to konieczne"
                                 placement="top">
                            <Button variant="contained" color="primary" size="large"
                                    onClick={() => this.onAnnotButtonClick(this.state.tweetId, ANNOTATION_SKIP)}
                                    style={{
                                        width: `${BUTTON_WIDTH}px`,
                                        height: `${BUTTON_HEIGHT}px`
                                    }}
                                    disabled={this.state.annotButtonsDisabled}>
                                <NavigateNextIcon fontSize="large"/>
                            </Button>
                        </Tooltip>
                    </div>
                </div>
                <DialogUsername
                    open={this.state.dialogOpen}
                    handleClose={() => this.handleCloseUsernameDialog()}
                    handleSubmitUsername={this.handleSubmitUsername.bind(this)}
                />

                <HelpDialog open={this.state.helpOpen} toggleDialog={this.toggleHelpDialog.bind(this)}/>
            </div>
        )
    }
}

export default MainScreen;
