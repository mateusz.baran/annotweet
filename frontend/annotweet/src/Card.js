import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from "@material-ui/core/CardContent/CardContent";
import Card from "@material-ui/core/Card/Card";

const useStyles = makeStyles({
    rootCard: {
        width: "90%",
        maxWidth: 800,
    },
});

export default function CardElement(props) {
    const classes = useStyles();

    return (
        <Card classes={classes.rootCard}>
            <CardContent>
                <Typography variant="h6">
                    {props.tweetId ? props.tweetText : props.errorText}
                </Typography>
            </CardContent>
        </Card>
    );
};