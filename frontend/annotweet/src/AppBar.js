import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import HelpIcon from '@material-ui/icons/Help';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from "@material-ui/core/IconButton/IconButton";
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import clsx from 'clsx';
import MenuDrawer from "./MenuDrawer";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    appBarShift: {
        zIndex: theme.zIndex.drawer - 1,
    },
    menuCounter: {
        paddingRight: "40px",
    },
    title: {
        flexGrow: 1,
    },
    help: {
        flexGrow: 0.1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
}));

export default function ButtonAppBar(props) {
    const classes = useStyles();
    const is_desktop = useMediaQuery('(min-width:1025px)');

    return (
        <div className={classes.root}>
            <AppBar position="fixed"
                    className={is_desktop
                        ? classes.appBar
                        : clsx(classes.appBar, {
                            [classes.appBarShift]: props.drawerOpen,
                        })}
            >
                <Toolbar>
                    {is_desktop
                        ? null
                        : (
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={props.handleDrawerOpen}
                                edge="start"
                                className={clsx(classes.menuButton)}
                            >
                                <MenuIcon/>
                            </IconButton>)}
                    <Typography variant="h5" className={classes.title}>Annotweet
                        <Tooltip title="Help / Pomoc"
                                 placement="bottom">
                            <IconButton aria-label="help" onClick={props.onHelpButtonClick}>
                                <HelpIcon style={{color: "white"}}/>
                            </IconButton>
                        </Tooltip>
                    </Typography>
                    <Tooltip title="Back to last tweet / Powrót do poprzedniego tweeta"
                             placement="bottom">
                        <IconButton aria-label="Back do last tweet" onClick={props.onBackToLastTweetButtonClick}>
                            <KeyboardBackspaceIcon style={{color: "white"}} size='large'/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Set username / Ustaw nazwę użytkownika"
                             placement="bottom">
                        <Button color="inherit" onClick={() => props.onUsernameButtonClick()}>
                            {props.username ? `@${props.username}` : '-'}
                        </Button>
                    </Tooltip>
                </Toolbar>
            </AppBar>
            <MenuDrawer open={props.drawerOpen} handleDrawerClose={props.handleDrawerClose}
                        {...props.mainState} />
        </div>
    );
}