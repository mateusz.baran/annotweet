from setuptools import find_packages, setup

from annotweet import __version__

setup(
    name='annotweet',
    version=__version__,
    packages=find_packages(),
    install_requires=[
        'aiofiles==0.6.0',
        'fastapi==0.61.1',
        'PyYAML==5.3.1',
        'pymongo==3.11.0',
        'python-jose[cryptography]==3.2.0',
        'python-multipart==0.0.5',
        'requests==2.25.0',
        'scikit-learn==0.23.2',
        'uvicorn==0.11.8',
    ],
    extras_require={
        'dev': [
            'pytest==6.0.2',
            'pytest-flake8==1.0.6',
            'pytest-isort==1.2.0',
        ],
    }
)
