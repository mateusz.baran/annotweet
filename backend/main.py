from annotweet.services.api import API
from annotweet.services.core import Core
from annotweet.services.database import Database
from annotweet.services.logger import Logger

logging = Logger.create()
core = Core.create()
database = Database.create()
api = API.create()

app = api.app

if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, log_config=None)
