from datetime import datetime, timedelta

from annotweet.services.database import Database

db = Database.create()

for col in db.database.list_collection_names():
    db.database.drop_collection(col)

db.dataset.collection.insert_many([
    {
        '_id': '1',
        'last_access': datetime.fromtimestamp(0).isoformat(),
        'text': "Post nr 1",
        'meta': {
            'url': "http://sm.pl"
        },
        'annotations': [
            {
                'username': 'Krzysiek',
                'answer': 'inducement',
                'labels': ['satire'],
                'date': (datetime.utcnow() - timedelta(minutes=1)).isoformat(),
            },
            {
                'username': 'Krzysiek',
                'answer': 'inducement',
                'labels': [],
                'date': datetime.utcnow().isoformat(),
            },
            {
                'username': 'Wojtek',
                'answer': 'skip',
                'labels': [],
                'date': datetime.utcnow().isoformat(),
            },
        ],
    },
    {
        '_id': '2',
        'last_access': datetime.fromtimestamp(0).isoformat(),
        'text': "Post nr 2",
        'meta': {
            'url': "http://sm.pl"
        },
        'annotations': [
            {
                'username': 'Krzysiek',
                'answer': 'skip',
                'labels': [],
                'date': datetime.utcnow().isoformat(),
            },
            {
                'username': 'Adam',
                'answer': 'inducement',
                'labels': ['missing_context', 'media_report'],
                'date': datetime.utcnow().isoformat(),
            },
        ],
    },
    {
        '_id': '3',
        'last_access': datetime.utcnow().isoformat(),
        'text': "Post nr 3",
        'meta': {
            'url': "http://sm.pl"
        },
        'annotations': [],
    },
    {
        '_id': '4',
        'last_access': datetime.fromtimestamp(0).isoformat(),
        'text': "Post nr 4",
        'meta': {
            'url': "http://sm.pl"
        },
        'annotations': [
            {
                'username': 'Wojtek',
                'answer': 'inducement',
                'labels': ['missing_context', 'satire'],
                'date': datetime.utcnow().isoformat(),
            },
        ],
    },
    {
        '_id': '5',
        'last_access': datetime.fromtimestamp(0).isoformat(),
        'text': "Post nr 5",
        'meta': {
            'url': "http://sm.pl"
        },
        'annotations': [
            {
                'username': 'Krzysiek',
                'answer': 'skip',
                'labels': [],
                'date': datetime.utcnow().isoformat(),
            },
            {
                'username': 'Wojtek',
                'answer': 'inducement',
                'labels': [],
                'date': datetime.utcnow().isoformat(),
            },
        ],
    },
    {
        '_id': '6',
        'last_access': datetime.fromtimestamp(0).isoformat(),
        'text': "Post nr 6",
        'annotations': [],
    },
])
