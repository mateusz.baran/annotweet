from annotweet.settings import FRONTEND_DIR, Config


class APIConfig(metaclass=Config):
    ROOT_PATH = None
    TITLE = "Tweet annotator"
    DESCRIPTION = "Web application for tweet annotating"

    ORIGINS = ['http://localhost:3000']

    STATIC_FILES_DIR = FRONTEND_DIR / 'annotweet' / 'build'

    TOKEN_URL = '/auth/token'

    def production(self):
        self.ROOT_PATH = '/api'
        self.TOKEN_URL = '/api/auth/token'

        self.ORIGINS = []
