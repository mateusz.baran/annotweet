from annotweet.settings import STORAGE_DIR, Config


class LoggerConfig(metaclass=Config):
    PATH = STORAGE_DIR / 'configs' / 'logging.yml'
    LOG_DIR = STORAGE_DIR / 'logs'
