from annotweet.settings import Config


class DatabaseConfig(metaclass=Config):
    NAME = 'annotweet'
    ADDRESS = 'mongodb://localhost:27017'
    DB_NAME = 'annotweetdb'

    DATASET_COLLECTION_NAME = 'dataset'

    def production(self):
        self.ADDRESS = 'mongodb://database:27017'
