from datetime import timedelta

from annotweet.settings import Config


class CoreConfig(metaclass=Config):
    RECORD_LOCK_MINUTES = timedelta(minutes=15)
    ANNOTATION_MODE = 1
    TWITTER_BASE_URL = 'https://publish.twitter.com/oembed?url=https://twitter.com/twitter/status/'
