from dataclasses import dataclass
from os import getenv
from pathlib import Path

PROJECT_DIR = Path(__file__).parent.parent.parent
STORAGE_DIR = PROJECT_DIR / '.storage'
FRONTEND_DIR = PROJECT_DIR.parent / 'frontend'

_ENVIRONMENTS = {'development', 'production'}
ENVIRONMENT = getenv('ANNOTWEET_ENVIRONMENT', default='default')


class Config(type):
    _instance = None

    def __new__(mcs, *args, **kwargs):
        cls = super(Config, mcs).__new__(mcs, *args, **kwargs)

        for env_name in _ENVIRONMENTS:
            if hasattr(cls, env_name):
                if env_name == ENVIRONMENT:
                    getattr(cls, env_name)(cls)
                delattr(cls, env_name)

        return dataclass(cls, init=False, frozen=True)

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(Config, cls).__call__(*args, **kwargs)
        return cls._instance

    def __setattr__(cls, key, value):
        if cls._instance is not None:
            raise ValueError("Config already created")
        return super(Config, cls).__setattr__(key, value)
