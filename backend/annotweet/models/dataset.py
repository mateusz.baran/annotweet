from datetime import datetime
from enum import Enum
from typing import Any, Dict, List

from pydantic import BaseModel, Field, constr


class UserIn(BaseModel):
    username: constr(min_length=3, max_length=32, strip_whitespace=True) = None

    class Config:
        extra = 'ignore'
        schema_extra = {
            'example': {
                'username': 'Adam',
            }
        }


class Answer(str, Enum):
    INDUCEMENT = 'inducement'
    ENCOURAGEMENT = 'encouragement'
    VOTING_TURNOUT = 'voting_turnout'
    NORMAL = 'normal'
    SKIP = 'skip'


class Labels(str, Enum):
    SATIRE = 'satire'
    MISSING_CONTEXT = 'missing_context'
    MEDIA_REPORT = 'media_report'


class Annotation(BaseModel):
    username: constr(min_length=3, max_length=32, strip_whitespace=True)
    answer: Answer
    labels: List[Labels] = Field(default_factory=list)
    date: datetime = Field(default_factory=lambda: datetime.utcnow())

    class Config:
        extra = 'ignore'
        use_enum_values = True
        schema_extra = {
            'example': {
                'username': 'Adam',
                'answer': 'inducement',
                'labels': ['missing_context', 'media_report'],
            }
        }


class Record(BaseModel):
    id: str = Field(alias='_id')
    text: constr(min_length=1, max_length=4096)
    meta: Dict[str, Any] = Field(default_factory=dict)
    last_access: datetime = Field(default_factory=lambda: datetime.fromtimestamp(0))
    annotations: List[Annotation] = Field(default_factory=list)

    class Config:
        extra = 'ignore'
        allow_population_by_field_name = True


class RecordOut(BaseModel):
    id: str = Field(alias='_id')
    text: constr(min_length=1, max_length=512)
    meta: Dict[str, Any] = Field(default_factory=dict)


class CreateDatasetMode(str, Enum):
    SAFE = 'safe'
    ADD = 'add'
    UPDATE = 'update'


class GetDatasetMode(str, Enum):
    ALL = 'all'
    DONE = 'done'
    STARTED = 'started'


class AnnotationStats(BaseModel):
    annotations: int = 0
    inducement: int = 0
    encouragement: int = 0
    voting_turnout: int = 0
    normal: int = 0
    skip: int = 0
    done: int = 0
    doing: int = 0
    todo: int = 0
    total: int = 0
    cohen_kappa: float = 0.

    class Config:
        extra = 'ignore'
