from pydantic import BaseModel


class Stats(BaseModel):
    inducement: int = 0
    encouragement: int = 0
    voting_turnout: int = 0
    normal: int = 0
    skip: int = 0
    total: int = 0

    class Config:
        extra = 'ignore'
