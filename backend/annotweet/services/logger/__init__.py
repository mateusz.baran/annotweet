import logging
import os
from logging.config import dictConfig

import yaml

from annotweet import settings
from annotweet.services import Service
from annotweet.settings.logger import LoggerConfig


class Logger(metaclass=Service):
    def __init__(self):
        self.config = LoggerConfig()
        self._logger = Logger.get(__name__)

        self.config.LOG_DIR.mkdir(parents=True, exist_ok=True)
        with self.config.PATH.open() as file:
            logging_config = yaml.full_load(file)
            dictConfig(logging_config)
            self._logger.debug("Loaded logging config %s: %s", self.config.PATH, logging_config)

        self._logger.debug("Environment variables: %s", dict(os.environ))
        self._logger.debug("Log directory: %s", self.config.LOG_DIR.resolve())
        self._logger.info("Environment: %s", settings.ENVIRONMENT)

    @classmethod
    def get(cls, name: str):
        return logging.getLogger(name)
