from pymongo import MongoClient

from annotweet.services import Service
from annotweet.settings.database import DatabaseConfig


class Database(metaclass=Service):
    def __init__(self):
        self.config = DatabaseConfig()

        self._client = MongoClient(self.config.ADDRESS)
        self.database = self._client[self.config.DB_NAME]

        from annotweet.services.database.collections import DatasetCollection

        self.dataset = DatasetCollection(self.database, self.config.DATASET_COLLECTION_NAME)
