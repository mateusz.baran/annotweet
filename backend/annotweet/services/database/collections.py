from datetime import datetime
from typing import List

from annotweet.models.dataset import Annotation, Record


class DatasetCollection:
    def __init__(self, database, collection_name: str):
        self.database = database
        self.collection_name = collection_name
        self.collection = self.database[collection_name]

    def get_all(self) -> List[Record]:
        return [Record(**d) for d in self.collection.find({})]

    def get_all_ids(self) -> List[str]:
        return [d['_id'] for d in self.collection.find({})]

    def delete(self, record_ids: List[str]) -> List[str]:
        result = self.collection.delete_many({'_id': {'$in': record_ids}})
        return result.deleted_count

    def delete_all(self) -> int:
        result = self.collection.delete_many({})
        return result.deleted_count

    def create(self, dataset: List[Record]) -> int:
        result = self.collection.insert_many([record.dict(by_alias=True) for record in dataset])
        return len(result.inserted_ids)

    def update_last_access(self, record_id: str) -> bool:
        result = self.collection.update_one(
            filter={'_id': record_id},
            update={
                '$set': {'last_access': datetime.utcnow().isoformat()},
            },
        )
        return bool(result.modified_count)

    def add_annotation(self, record_id: str, annotation: Annotation) -> bool:
        result = self.collection.update_one(
            filter={'_id': record_id},
            update={
                '$push': {'annotations': annotation.dict()},
            },
        )
        return bool(result.modified_count)
