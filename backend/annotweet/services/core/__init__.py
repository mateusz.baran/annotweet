from annotweet.services import Service
from annotweet.settings.core import CoreConfig


class Core(metaclass=Service):
    def __init__(self):
        self.config = CoreConfig()

        from annotweet.services.core.record import RecordManager
        from annotweet.services.core.user import UserManager

        self.record = RecordManager(annotation_mode=self.config.ANNOTATION_MODE,
                                    record_lock_time=self.config.RECORD_LOCK_MINUTES,
                                    twitter_base_url=self.config.TWITTER_BASE_URL)
        self.user = UserManager()
