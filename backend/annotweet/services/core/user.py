from collections import defaultdict
from typing import List

from annotweet.models.dataset import Record
from annotweet.models.user import Stats


class UserManager:
    @staticmethod
    def get_stats(username: str, dataset: List[Record]) -> Stats:
        stats = defaultdict(int)

        for record in dataset:
            for annotation in reversed(record.annotations):
                if annotation.username == username:
                    stats[annotation.answer] += 1
                    stats['total'] += 1
                    break

        return Stats(**stats)

    @staticmethod
    def get_annotations(username: str, dataset: List[Record]) -> List[Record]:
        new_dataset = list()
        for record in dataset:
            annotations = [annot for annot in record.annotations if annot.username == username]
            if annotations:
                new_record = record.copy()
                new_record.annotations = annotations
                new_dataset.append(new_record)

        return new_dataset

    @staticmethod
    def get_list(dataset: List[Record]) -> List[str]:
        return list(set(annot.username for record in dataset for annot in record.annotations))
