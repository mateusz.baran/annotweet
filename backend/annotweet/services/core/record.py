import itertools
import random
from collections import defaultdict
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional, Tuple

from sklearn.metrics import cohen_kappa_score

from annotweet.models.dataset import Annotation, AnnotationStats, Answer, Record


class RecordManager:
    def __init__(self, annotation_mode: int, record_lock_time: timedelta, twitter_base_url: str):
        self._annotation_mode = annotation_mode
        self._record_lock_time = record_lock_time
        self._twitter_base_url = twitter_base_url

    @staticmethod
    def _most_common_annotation(annotations: List[Annotation]) -> Tuple[Answer, int]:
        counts = defaultdict(int)
        for annotation in {annotation.username: annotation for annotation in annotations}.values():
            counts[annotation.answer] += 1

        return max(counts.items(), key=lambda x: x[1], default=(None, 0))

    def get_random_record(self, username: str, dataset: List[Record]) -> Optional[Record]:
        mca = {
            r.id: self._most_common_annotation(r.annotations)[1] for r in dataset
        }

        now = datetime.utcnow()
        dataset = [record for record in dataset
                   if now - record.last_access > self._record_lock_time
                   and mca[record.id] < self._annotation_mode
                   and username not in (annotation.username for annotation in record.annotations)]

        dataset_annotation_count = [mca[record.id] for record in dataset]
        dataset_annotation_max = max(dataset_annotation_count, default=0)
        dataset = [record for record in dataset if mca[record.id] == dataset_annotation_max]

        if dataset:
            return random.choice(dataset)

    def get_annotation_stats(self, dataset: List[Record]) -> AnnotationStats:
        stats = defaultdict(int)
        users = defaultdict(list)

        mca = {
            record.id: self._most_common_annotation(record.annotations) for record in dataset
        }

        for record in dataset:
            mca_answer, mca_count = mca[record.id]

            if mca_count >= self._annotation_mode:
                stats[mca_answer] += 1
                stats['done'] += 1
            elif mca_count:
                stats['doing'] += 1
            else:
                stats['todo'] += 1

            annots = list({annotation.username: annotation.answer
                           for annotation in record.annotations
                           if annotation.answer != Answer.SKIP}.values())

            if len(annots) >= 2 and mca_answer != Answer.SKIP:
                users['first'].append(annots[0])
                users['second'].append(annots[1])

            stats['annotations'] += len(record.annotations)
            stats['total'] += 1

        labels = [a.value for a in Answer if a != Answer.SKIP]
        cohen_kappa = (cohen_kappa_score(users['first'], users['second'], labels=labels)
                       if set(users['first']).intersection(users['second']) == set(labels) else 0.)

        return AnnotationStats(**stats, cohen_kappa=cohen_kappa)

    def filter_done_records(self, dataset: List[Record]) -> List[Record]:
        mca = {r.id: self._most_common_annotation(r.annotations)[1] for r in dataset}
        done_records = [record for record in dataset if mca[record.id] >= self._annotation_mode]
        return done_records

    def filter_started_records(self, dataset: List[Record]) -> List[Record]:
        mca = {r.id: self._most_common_annotation(r.annotations)[1] for r in dataset}
        started_records = [record for record in dataset if mca[record.id]]
        return started_records

    def get_cohen_kappa_matrix(self, dataset: List[Record]) -> Dict[str, Dict[str, float]]:
        user_set = set()
        annot_pairs = list()
        matrix = defaultdict(dict)

        mca = {record.id: self._most_common_annotation(record.annotations) for record in dataset}

        for record in dataset:
            mca_answer, mca_count = mca[record.id]
            if mca_count >= 2 and mca_answer != Answer.SKIP:
                annot_1, annot_2 = list({annotation.username: annotation
                                         for annotation in record.annotations
                                         if annotation.answer != Answer.SKIP}.values())[:2]
                annot_pairs.append((annot_1, annot_2))
                user_set.add(annot_1.username)
                user_set.add(annot_2.username)

        labels = [a.value for a in Answer if a != Answer.SKIP]
        for user_1, user_2 in itertools.combinations(user_set, r=2):
            u_set = {user_1, user_2}
            ats1 = [a1.answer for a1, a2 in annot_pairs if {a1.username, a2.username} == u_set]
            ats2 = [a2.answer for a1, a2 in annot_pairs if {a1.username, a2.username} == u_set]

            if set(ats1).intersection(ats2) == set(labels):
                matrix[user_1][user_2] = cohen_kappa_score(ats1, ats2, labels=labels), len(ats1)

        return matrix

    def get_mistakes(self, dataset: List[Record]) -> Dict[str, Dict[str, Any]]:
        stats: Dict[str, Dict[str, Any]] = {
            'global_total': defaultdict(lambda: defaultdict(int)),
            'user_total': defaultdict(lambda: defaultdict(lambda: defaultdict(int))),
            'global': defaultdict(lambda: defaultdict(lambda: defaultdict(int))),
            'user': defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int)))),
        }

        mca = {record.id: self._most_common_annotation(record.annotations) for record in dataset}

        for record in dataset:
            mca_answer, mca_count = mca[record.id]
            if mca_count >= self._annotation_mode and mca_answer != Answer.SKIP:
                record_annotations = {annotation.username: annotation.answer
                                      for annotation in record.annotations
                                      if annotation.answer != Answer.SKIP}

                for username, annotation in record_annotations.items():
                    if annotation != mca_answer:
                        stats['global_total'][annotation]['mistakes'] += 1
                        stats['global'][annotation][mca_answer]['mistakes'] += 1
                        stats['user_total'][username][annotation]['mistakes'] += 1
                        stats['user'][username][annotation][mca_answer]['mistakes'] += 1

                    stats['global_total'][annotation]['answers'] += 1
                    stats['user_total'][username][annotation]['answers'] += 1

        return stats
