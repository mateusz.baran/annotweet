class Service(type):
    _services = dict()

    def create(cls):
        if cls not in cls._services:
            cls._services[cls] = super(Service, cls).__call__()
            return cls._services[cls]
        else:
            raise RuntimeError("Service is already created")

    def __call__(cls):
        if cls not in cls._services:
            raise RuntimeError("Service is not created")
        return cls._services[cls]
