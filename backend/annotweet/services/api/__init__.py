from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from annotweet import __version__
from annotweet.services import Service
from annotweet.settings.api import APIConfig


class API(metaclass=Service):
    def __init__(self):
        self.config = APIConfig()

        self.app = FastAPI(
            root_path=self.config.ROOT_PATH,
            title=self.config.TITLE,
            description=self.config.DESCRIPTION,
            version=__version__,
        )

        self.app.add_middleware(
            CORSMiddleware,
            allow_origins=self.config.ORIGINS,
            allow_credentials=True,
            allow_methods=['*'],
            allow_headers=['*'],
        )

        from annotweet.services.api.routers import dataset, record, user

        self.app.include_router(record.router, prefix='/text', tags=['text'])
        self.app.include_router(dataset.router, prefix='/dataset', tags=['dataset'])
        self.app.include_router(user.router, prefix='/user', tags=['user'])

        if self.config.STATIC_FILES_DIR.exists():
            self.app.mount(path="/",
                           app=StaticFiles(directory=self.config.STATIC_FILES_DIR, html=True),
                           name="frontend")
