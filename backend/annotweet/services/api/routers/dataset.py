import json
from typing import List

from fastapi import APIRouter, Depends, File, UploadFile
from pydantic import ValidationError

from annotweet.models.dataset import CreateDatasetMode, GetDatasetMode, Record
from annotweet.services.api.utils.exceptions import (DatabaseNotEmptyExp,
                                                     DatasetIndicesNotUniqueExp, ExceptionModel,
                                                     ImproperJsonKeysExc, JSONDecodeExp)
from annotweet.services.core import Core
from annotweet.services.database import Database
from annotweet.services.logger import Logger

router = APIRouter()
logger = Logger.get(__name__)


@router.get('', response_model=List[Record])
def get_dataset(mode: GetDatasetMode, db=Depends(Database), core=Depends(Core)):
    dataset = db.dataset.get_all()

    if mode == GetDatasetMode.STARTED:
        dataset = core.record.filter_started_records(dataset)
    elif mode == GetDatasetMode.DONE:
        dataset = core.record.filter_done_records(dataset)

    return dataset


@router.delete('')
def delete_dataset(db=Depends(Database)):
    return db.dataset.delete_all()


@router.put('', responses=ExceptionModel.responses(DatabaseNotEmptyExp(), ImproperJsonKeysExc(),
                                                   DatasetIndicesNotUniqueExp(), JSONDecodeExp()))
async def load_dataset(mode: CreateDatasetMode, file: UploadFile = File(...), db=Depends(Database)):
    try:
        json_data = json.loads(await file.read())
    except json.JSONDecodeError:
        raise JSONDecodeExp()

    try:
        dataset = [Record(**line) for line in json_data]
    except ValidationError:
        raise ImproperJsonKeysExc()

    unique_ids = set(record.id for record in dataset)
    if len(unique_ids) != len(dataset):
        raise DatasetIndicesNotUniqueExp()

    existing_ids = db.dataset.get_all_ids()
    if mode == CreateDatasetMode.SAFE and existing_ids:
        raise DatabaseNotEmptyExp()
    elif mode == CreateDatasetMode.ADD:
        dataset = [record for record in dataset if record.id not in existing_ids]
    elif mode == CreateDatasetMode.UPDATE:
        records_to_remove = set([record.id for record in dataset]).intersection(existing_ids)
        if records_to_remove:
            deleted_count = db.dataset.delete(list(records_to_remove))
            logger.debug("Deleted %s records", deleted_count)

    return db.dataset.create(dataset) if dataset else []
