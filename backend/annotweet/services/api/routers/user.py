from typing import List

from fastapi import APIRouter, Depends

from annotweet.models.dataset import Record, UserIn
from annotweet.models.user import Stats
from annotweet.services.core import Core
from annotweet.services.database import Database

router = APIRouter()


@router.post('/stats', response_model=Stats)
def get_stats(user: UserIn, core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    stats = core.user.get_stats(user.username, dataset)
    return stats


@router.post('/annotations', response_model=List[Record])
def get_annotations(user: UserIn, core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    annotations = core.user.get_annotations(user.username, dataset)
    return annotations


@router.get('/list')
def get_list(core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    user_list = core.user.get_list(dataset)
    return user_list
