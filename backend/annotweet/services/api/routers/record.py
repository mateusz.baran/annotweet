from fastapi import APIRouter, Depends

from annotweet.models.dataset import Annotation, AnnotationStats, RecordOut, UserIn
from annotweet.services.api.utils.exceptions import ExceptionModel, RecordTemporarilyUnavailable
from annotweet.services.core import Core
from annotweet.services.database import Database
from annotweet.services.logger import Logger

router = APIRouter()
logger = Logger.get(__name__)


@router.post('/get', response_model=RecordOut,
             responses=ExceptionModel.responses(RecordTemporarilyUnavailable()))
def get(user: UserIn, core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    random_record = core.record.get_random_record(user.username, dataset)

    if not random_record:
        raise RecordTemporarilyUnavailable()

    db.dataset.update_last_access(random_record.id)
    return random_record


@router.patch('/annotate/{record_id}')
def annotate(record_id: str, annotation: Annotation, db=Depends(Database)):
    return db.dataset.add_annotation(record_id, annotation)


@router.get('/annotation_stats', response_model=AnnotationStats)
def get_stats(core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    stats = core.record.get_annotation_stats(dataset)
    return stats


@router.get('/cohen_kappa_matrix')
def get_cohen_kappa_matrix(core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    cohen_kappa_matrix = core.record.get_cohen_kappa_matrix(dataset)
    return cohen_kappa_matrix


@router.get('/mistakes')
def get_mistakes(core=Depends(Core), db=Depends(Database)):
    dataset = db.dataset.get_all()
    mistakes = core.record.get_mistakes(dataset)
    return mistakes
