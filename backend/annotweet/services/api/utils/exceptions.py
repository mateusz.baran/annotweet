from typing import Dict

from fastapi import HTTPException, status
from pydantic import BaseModel


class ExceptionModel(BaseModel):
    detail: str

    @classmethod
    def responses(cls, *exceptions: HTTPException) -> Dict[int, Dict[str, 'ExceptionModel']]:
        return {exc.status_code: {'model': cls} for exc in exceptions}


class DatabaseNotEmptyExp(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail="Database must be empty")


class DatasetIndicesNotUniqueExp(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                         detail="Database indices must be unique")


class JSONDecodeExp(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                         detail="Requires json file")


class ImproperJsonKeysExc(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                         detail="Json must contain keys `_id` and `text`")


class RecordTemporarilyUnavailable(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_404_NOT_FOUND,
                         detail="Record temporarily unavailable")
